var gulp           = require('gulp'),
    concat         = require('gulp-concat'),       // Склейка файлов
    less           = require('gulp-less'),         // Плагин для less
    path           = require('path'),              // Плагин для less
    autoprefixer   = require('gulp-autoprefixer'), // Автопрефиксер
    browserSync    = require('browser-sync'),      // BrowserSync
    //rigger       = require('gulp-rigger'),       // Rigger - Объединение файлов по ссылкам
    uglify         = require('gulp-uglify'),       // Uglify - Минификация js
    svgmin         = require('gulp-svgmin'),       // Минификация SVG
    bower          = require('gulp-bower'),
    mainBowerFiles = require('main-bower-files');

// Автообновление/сервер
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "build"
        }
    });

    gulp.watch("dev/html/**/*").on('change', browserSync.reload);
    gulp.watch("dev/less/*.less").on('change', browserSync.reload);
    gulp.watch("dev/js/**/*").on('change', browserSync.reload);
});
gulp.task('bs-reload', function () {
    browserSync.reload();
});

// Сборка html файлов
gulp.task('html', function () {
    gulp.src('dev/html/*.html') //Выберем файлы по нужному пути
        //.pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest('build/')); //Выплюнем их в папку build
});


// Сбор less & -> css
gulp.task('css', function() {
    gulp.src('dev/less/*.less')
        .pipe(concat('styles.less'))
        //.pipe(sourcemaps.init())
        .pipe(less({
          paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(autoprefixer({
          browsers: ['> 1.1%']
        }))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'));
});

// Сбор js
gulp.task('js', function() {
    gulp.src('dev/js/**/*')
        .pipe(gulp.dest('build/js'));
});

console.log(mainBowerFiles())

gulp.task('bower', function() {
    var vendors = mainBowerFiles();
    gulp.src(vendors)
        .pipe(gulp.dest('build/js/lib'));
});

// Сбор img
gulp.task('image', function() {
    gulp.src('dev/img/**/*')
        .pipe(gulp.dest('build/img/'));
});


// Сбор и минификация svg
gulp.task('svg', function () {
    gulp.src('dev/svg/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest('build/img/svg'));
});


// default
gulp.task('default', ['html','css','js','bower','browser-sync'], function () {
    gulp.watch("dev/html/**/*.html", ['bs-reload','html']);
    gulp.watch("dev/less/*.less", ['bs-reload','css']);
    gulp.watch("dev/js/**/*", ['bs-reload','js']);
    gulp.watch("bower_components/**/*", ['bs-reload','bower']);
});
