var pageApp = angular.module('pageApp', []);

pageApp.controller('DataListCtrl', function($scope) {
    $scope.rows = [
        {
            "type" : "Персональное",
              "id" : 16561,
            "city" : "Архангельск",
            "desc" : "Подключение провод от клеммной коробки пожарной сигнализации к расцепителю, проверить работоспособность",
            "date" : "13 августа",
           "price" : "10 200"
        },
        {
            "type" : "Ночное",
              "id" : 16562,
            "city" : "Самара",
            "desc" : "Локальный сметный расчет электроснабжения 6кВ воздушной линии, включая: демонтажные работы, демонтаж опор ВЛ 0.38-10 кВ без приставок одностоечных с подкосом",
            "date" : "3 сентября",
           "price" : "40 150"
        },
        {
            "type" : "Срочное",
              "id" : 16563,
            "city" : "Москва",
            "desc" : "Монтаж и эксплуатация средств энергоснабжения аппаратуры должны соответствовать правилам и нормам \"Правил устройства электроустановок\" (ПУЭ).",
            "date" : "22 октября",
           "price" : "3 220"
        }
    ];


    $scope.filter = {};

    $scope.getTypes = function () {
        return ($scope.rows || []).map(function (item) {
            return item.type;
        });
    };

    $scope.filterByCategory = function (row) {
        return $scope.filter[row.type] || noFilter($scope.filter);
    };
    
    function noFilter(filterObj) {
        for (var key in filterObj) {
            if (filterObj[key]) {
                return false;
            }
        }
        return true;
    } 
});