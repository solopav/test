//Поиск в строках таблицы
function searchCheckbox(input) {
  var items = $('.js-filter-item');
  var searchVal = input.val().toLowerCase();
  var number = searchVal.length;

  items.each(function() {
    if ($(this).hasClass('checked')) {
      $row = $(this);

      var text = $row.find("td").text().toLowerCase();

      if (text.indexOf(searchVal) == -1) {
        $(this).hide();
      }
      else {
        $(this).show();
      }
    }
  });
};

// Вкл/выкл типы
function chechCheckbox() {
  var types = [];
  $('.js-checkbox:checked').each(function  () {
    types.push($(this).val());
  });

  $('.js-filter-item').each(function() {
    var dataType = $(this).attr('data-type');

    if (types.length) {
      if (types.indexOf(dataType) !== -1) {
        $(this).addClass('checked').show();
      } else {
        $(this).removeClass('checked').hide();
      }
    } else {
      $('.js-filter-item').addClass('checked').show();
    }
  });
}


// Подчеркивание таба
// width/position line
function calculation(that) {
  var widthLine = that.outerWidth();
  var postionLine = that.position().left;


  $('.js-line').css({
    'left' : postionLine + 10,
    'width' : widthLine - 20
  });
}

// line search active after document.ready
function searchActive() {
  $('.js-tab').each(function() {
    if ( $(this).hasClass('active') ) {
      var that = $(this);
      calculation( that );
      return false;
    } else {
      $(line).css('width',0);
    }
  });
}



$(document).ready(function() {
  // Search active
  searchActive();

  $('.js-tab').hover(
    function() {
      var that = $(this);
      calculation( that );
    }, 
    function() {
      searchActive();
    }
  );

  // Событие изменения в инпуте
  $('.js-filter-search').on('cut change keyup paste blur', function() {
    searchCheckbox($(this));
  });

  // Событие изменения чекбокса
  $('.js-checkbox').change(function() {
    chechCheckbox();
    searchCheckbox($('.js-filter-search'));
  });
});